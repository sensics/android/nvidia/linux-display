/*
 * panel-b-1440x1600-35.c: Panel driver for b-1440x1600-35 panel.
 *
 * Copyright (c) 2013-2017, NVIDIA CORPORATION. All rights reserved.
 * Copyright (c) 2018, Sensics, Inc. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/of_gpio.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/regulator/consumer.h>

#include "../dc.h"
#include "board.h"
#include "board-panel.h"

#define DC_CTRL_MODE TEGRA_DC_OUT_CONTINUOUS_MODE
#define DSI_PANEL_RESET 1

struct amati_panel_options {
	const char *compat_string;
	const char *node_name;
	bool dual_panel;
	bool dual_link;
};

struct amati_panel_device {
	bool populated;
	bool dual_reset;
	struct amati_panel_options const *opts;
	/* TEGRA_N_GPIO_PANEL counts of gpio should be
	 * initialized to TEGRA_GPIO_INVALID */
	struct tegra_panel_of panel_of;
	struct regulator *dvdd_lcd_1v8;
	struct regulator *avdd_lcd[2];
	struct regulator *avee_lcd[2];
	struct regulator *vdd_lcd_bl_en;
	struct device *dc_dev;
	u16 en_panel_rst[2];
};

#define AMATI_PANEL_NULL_INSTANCE                                              \
	{                                                                      \
		.populated = false, .dual_reset = false, .opts = NULL,         \
		.panel_of =                                                    \
			{                                                      \
				.panel_gpio = {-1, -1, -1, -1, -1, -1, -1},    \
			},                                                     \
		.dvdd_lcd_1v8 = NULL, .avdd_lcd = {NULL, NULL},                \
		.avee_lcd = {NULL, NULL}, .vdd_lcd_bl_en = NULL,               \
		.dc_dev = NULL, .en_panel_rst = {-EINVAL, -EINVAL},            \
	}

/* Can only be instantiated on DC 0 and 1 */
#define AMATI_MAX_PANELS 2
struct amati_panel_device amati_panel_devs[AMATI_MAX_PANELS] = {
	AMATI_PANEL_NULL_INSTANCE,
	AMATI_PANEL_NULL_INSTANCE,
};

enum { AMATI_PANEL_A,
       AMATI_PANEL_B,
       AMATI_DUAL_LINK,
       AMATI_NUM_PANEL_OPTIONS /* Must be last! */
};

const struct amati_panel_options amati_panel_opts[AMATI_NUM_PANEL_OPTIONS] = {
	[AMATI_PANEL_A] = {.compat_string = "b,1440x1600-35-a",
			   .node_name = "panel-b-1440x1600-35-a",
			   .dual_panel = false,
			   .dual_link = false},
	[AMATI_PANEL_B] = {.compat_string = "b,1440x1600-35-b",
			   .node_name = "panel-b-1440x1600-35-b",
			   .dual_panel = false,
			   .dual_link = false},
	[AMATI_DUAL_LINK] = {.compat_string = "b,1440x1600-35-duallink",
			     .node_name = "panel-b-1440x1600-35-duallink",
			     .dual_panel = true,
			     .dual_link = true},
};

static struct regulator *
amati_regulator_get_2(struct device *dev, const char *name1, const char *name2)
{
	struct regulator *temp = devm_regulator_get_optional(dev, name1);
	if (temp == ERR_PTR(-EPROBE_DEFER)) {
		dev_info(
			dev,
			"*** Regulator by name %s found but not set up yet - probe defer\n",
			name1);
		return temp;
	} else if (!IS_ERR(temp)) {
		dev_info(dev, "*** Found regulator by name %s\n", name1);
		return temp;
	}
	dev_info(dev, "*** Could not find regulator by name %s\n", name1);
	temp = devm_regulator_get_optional(dev, name2);
	if (temp == ERR_PTR(-EPROBE_DEFER)) {
		dev_info(
			dev,
			"*** Regulator by name %s found but not set up yet - probe defer\n",
			name2);
		return temp;
	} else if (IS_ERR(temp)) {
		dev_info(dev, "*** Could not find regulator by name %s\n",
			 name2);
	} else {
		dev_info(dev, "*** Found regulator by name %s\n", name2);
	}
	return temp; /* whether or not it's an error */
}
static struct regulator *amati_regulator_get_3(struct device *dev,
					       const char *name1,
					       const char *name2,
					       const char *name3)
{
	struct regulator *temp = amati_regulator_get_2(dev, name1, name2);
	if (!IS_ERR(temp) || temp == ERR_PTR(-EPROBE_DEFER)) {
		return temp;
	}
	temp = devm_regulator_get_optional(dev, name3);
	if (temp == ERR_PTR(-EPROBE_DEFER)) {
		dev_info(
			dev,
			"*** Regulator by name %s found but not set up yet - probe defer\n",
			name3);
	} else if (IS_ERR(temp)) {
		dev_info(dev, "*** Could not find regulator by name %s\n",
			 name3);
	} else {
		dev_info(dev, "*** Found regulator by name %s\n", name3);
	}
	return temp; /* whether or not it's an error */
}

#define amati_panel_err(panel_dev, format, ...)                                \
	dev_err(panel_dev->dc_dev, "Panel %s (%s): " format,                   \
		panel_dev->opts->compat_string, panel_dev->opts->node_name,    \
		##__VA_ARGS__)
#define amati_panel_warn(panel_dev, format, ...)                               \
	dev_warn(panel_dev->dc_dev, "Panel %s (%s): " format,                  \
		 panel_dev->opts->compat_string, panel_dev->opts->node_name,   \
		 ##__VA_ARGS__)
#define amati_panel_info(panel_dev, format, ...)                               \
	dev_info(panel_dev->dc_dev, "Panel %s (%s): " format,                  \
		 panel_dev->opts->compat_string, panel_dev->opts->node_name,   \
		 ##__VA_ARGS__)

static int dsi_b_1440x1600_35_find_resets(struct amati_panel_device *panel_dev)
{
	int err = 0;
	struct amati_panel_options const *opts = panel_dev->opts;
	struct device_node *node =
		of_find_compatible_node(NULL, NULL, opts->compat_string);
	if (!node) {
		amati_panel_warn(
			panel_dev,
			"Panel not found in DT - should not happen!\n");
		err = -ENOENT;
		goto fail;
	}
	if (opts->dual_panel) {
		panel_dev->en_panel_rst[1] =
			of_get_named_gpio(node, "nvidia,panel-rst-gpio", 1);

		if (gpio_is_valid(panel_dev->en_panel_rst[1])) {
			gpio_request(panel_dev->en_panel_rst[1],
				     "tegra-panel-reset-b");
			amati_panel_info(
				panel_dev,
				"found second reset GPIO as expected, handling dual panels\n");
			panel_dev->dual_reset = true;
		} else {
			amati_panel_info(
				panel_dev,
				"Expected two reset GPIOs (for two panels), but didn't find second one. Using just one.\n");
		}
	} else {
		amati_panel_info(
			panel_dev,
			"handling only single panel, not looking for second reset GPIO\n");
	}
fail:
	of_node_put(node);
	return err;
}
static int amati_dsi_panel_get_regulators(struct amati_panel_device *panel_dev)
{
	int err = 0;
	struct device *dev = panel_dev->dc_dev;
	panel_dev->dvdd_lcd_1v8 = regulator_get(dev, "dvdd_lcd");
	if (IS_ERR(panel_dev->dvdd_lcd_1v8)) {
		amati_panel_err(panel_dev, "dvdd_lcd regulator get failed\n");
		err = PTR_ERR(panel_dev->dvdd_lcd_1v8);
		panel_dev->dvdd_lcd_1v8 = NULL;
		goto fail;
	}

	panel_dev->avdd_lcd[0] =
		amati_regulator_get_3(dev, "outp", "avdd_a", "avdd");
	if (IS_ERR(panel_dev->avdd_lcd[0])) {
		amati_panel_err(panel_dev,
				"outp/avdd_a/avdd-lcd regulator get failed\n");
		err = PTR_ERR(panel_dev->avdd_lcd[0]);
		panel_dev->avdd_lcd[0] = NULL;
		goto fail;
	}

	panel_dev->avee_lcd[0] =
		amati_regulator_get_3(dev, "outn", "avee_a", "avee-lcd");
	if (IS_ERR(panel_dev->avee_lcd[0])) {
		amati_panel_err(panel_dev,
				"outn/avee_a/avee-lcd regulator get failed\n");
		err = PTR_ERR(panel_dev->avee_lcd[0]);
		panel_dev->avee_lcd[0] = NULL;
		goto fail;
	}

	if (panel_dev->opts->dual_panel) {
		/* Only bother to look for these if we have two panels.
		 * Even then, it's OK if they aren't there - they might be
		 * combined
		 */

		panel_dev->avdd_lcd[1] =
			amati_regulator_get_2(dev, "outp_b", "avdd_b");
		if (IS_ERR(panel_dev->avdd_lcd[1])) {
			amati_panel_warn(
				panel_dev,
				"outp_b/avdd_b regulator get failed, assuming outp/avdd_a/avdd-lcd controls both panels\n");
			panel_dev->avdd_lcd[1] = NULL;
		}

		panel_dev->avee_lcd[1] =
			amati_regulator_get_2(dev, "outn_b", "avee_b");
		if (IS_ERR(panel_dev->avee_lcd[1])) {
			amati_panel_warn(
				panel_dev,
				"outn_b/avee_b regulator get failed, assuming outn/avee_a/avee-lcd controls both panels\n");
			panel_dev->avee_lcd[1] = NULL;
		}
	}

	panel_dev->vdd_lcd_bl_en =
		amati_regulator_get_2(dev, "vdd_lcd_bl_en", "vdd-sys-bl");
	if (IS_ERR(panel_dev->vdd_lcd_bl_en)) {
		amati_panel_err(
			panel_dev,
			"vdd_lcd_bl_en/vdd-sys-bl regulator get failed\n");
		err = PTR_ERR(panel_dev->vdd_lcd_bl_en);
		panel_dev->vdd_lcd_bl_en = NULL;
		goto fail;
	}
	return 0;
fail:
	return err;
}
static int amati_dsi_panel_populate(struct amati_panel_device *panel_dev,
				    struct device *dev,
				    struct amati_panel_options const *opts)
{
	int err = 0;
	if (panel_dev->populated)
		return 0;

	panel_dev->opts = opts;
	panel_dev->dc_dev = dev;
	amati_panel_info(panel_dev, "*** In amati_dsi_panel_populate");
	err = amati_dsi_panel_get_regulators(panel_dev);
	if (err < 0) {
		amati_panel_err(panel_dev, "regulator request failed\n");
		goto fail;
	}

	err = tegra_panel_gpio_get_dt(panel_dev->opts->compat_string,
				      &panel_dev->panel_of);
	if (err < 0) {
		amati_panel_err(panel_dev, "dsi gpio request failed\n");
		goto fail;
	}

	/* If panel gpios are specified in device tree,
	 * use that.
	 */
	if (gpio_is_valid(panel_dev->panel_of.panel_gpio[TEGRA_GPIO_RESET])) {
		panel_dev->en_panel_rst[0] =
			panel_dev->panel_of.panel_gpio[TEGRA_GPIO_RESET];
	} else {
		amati_panel_err(panel_dev, "dsi reset gpio not specified\n");
		goto fail;
	}

	err = dsi_b_1440x1600_35_find_resets(panel_dev);
	if (err < 0) {
		amati_panel_err(panel_dev,
				"dsi_b_1440x1600_35_find_resets failed\n");
		goto fail;
	}

	BUG_ON(!gpio_is_valid(panel_dev->en_panel_rst[0]));
	if (panel_dev->dual_reset) {
		BUG_ON(!gpio_is_valid(panel_dev->en_panel_rst[1]));
	}

	panel_dev->populated = true;
	amati_panel_info(
		panel_dev,
		"Completed getting the regulators/gpios without errors!\n");
	return 0;
fail:
	return err;
}

static void dsi_b_1440x1600_35_resetpanels(struct amati_panel_device *panel_dev,
					   bool in_reset)
{
	u8 val = in_reset ? 0 : 1;
	gpio_direction_output(panel_dev->en_panel_rst[0], val);
	if (panel_dev->dual_reset) {
		gpio_direction_output(panel_dev->en_panel_rst[1], val);
	}
}

static int dsi_b_1440x1600_35_enable(struct amati_panel_device *panel_dev,
				     struct device *dev,
				     struct amati_panel_options const *opts)
{
	int err = 0;
	dev_info(dev, "*** In dsi_b_1440x1600_35_enable");

	err = amati_dsi_panel_populate(panel_dev, dev, opts);
	if (err < 0) {
		dev_err(dev,
			"*** Could not populate panel device for node name %s, compatible string %s\n",
			opts->node_name, opts->compat_string);
		return err;
	}

	msleep(10);
	dsi_b_1440x1600_35_resetpanels(panel_dev, true);
	msleep(10);
	dsi_b_1440x1600_35_resetpanels(panel_dev, false);
	msleep(10);
	dsi_b_1440x1600_35_resetpanels(panel_dev, true);
	msleep(10);
	if (panel_dev->dvdd_lcd_1v8) {
		err = regulator_enable(panel_dev->dvdd_lcd_1v8);
		if (err < 0) {
			pr_err("dvdd_lcd regulator enable failed\n");
			goto fail;
		}
	}

	usleep_range(1000, 1200);
	if (panel_dev->avdd_lcd[0]) {
		err = regulator_enable(panel_dev->avdd_lcd[0]);
		if (err < 0) {
			amati_panel_err(
				panel_dev,
				"avdd_lcd[0] regulator enable failed\n");
			goto fail;
		}
	}
	if (panel_dev->avdd_lcd[1] && panel_dev->opts->dual_panel) {
		err = regulator_enable(panel_dev->avdd_lcd[1]);
		if (err < 0) {
			amati_panel_err(
				panel_dev,
				"avdd_lcd[1] regulator enable failed\n");
			goto fail;
		}
	}
	if (panel_dev->avee_lcd[0]) {
		err = regulator_enable(panel_dev->avee_lcd[0]);
		if (err < 0) {
			amati_panel_err(
				panel_dev,
				"avdd_lcd[1] regulator enable failed\n");
			goto fail;
		}
	}
	if (panel_dev->avee_lcd[1] && panel_dev->opts->dual_panel) {
		err = regulator_enable(panel_dev->avee_lcd[1]);
		if (err < 0) {
			amati_panel_err(
				panel_dev,
				"avee_lcd[1] regulator enable failed\n");
			goto fail;
		}
	}

	msleep(10);
	dsi_b_1440x1600_35_resetpanels(panel_dev, false);
	msleep(120);

	dev_info(dev, "*** Done with dsi_b_1440x1600_35_enable");
	/*** @todo run the DSI setup commands then wait a while and turn on the
	 * backlight! */
	return 0;
fail:
	return err;
}

static int dsi_b_1440x1600_35_disable(struct amati_panel_device *panel_dev)
{
	amati_panel_info(panel_dev, "*** In dsi_b_1440x1600_35_disable");
	if (panel_dev->vdd_lcd_bl_en) {
		regulator_disable(panel_dev->vdd_lcd_bl_en);
	}

	/* tSLPIN*/
	usleep_range(60000, 65000);


	dsi_b_1440x1600_35_resetpanels(panel_dev, true);

	/* Some additional time for safety */
	usleep_range(400, 500);
	if (panel_dev->avee_lcd[0]) {
		regulator_disable(panel_dev->avee_lcd[0]);
	}
	if (panel_dev->avee_lcd[1]) {
		regulator_disable(panel_dev->avee_lcd[1]);
	}
	if (panel_dev->avdd_lcd[0]) {
		regulator_disable(panel_dev->avdd_lcd[0]);
	}
	if (panel_dev->avdd_lcd[1]) {
		regulator_disable(panel_dev->avdd_lcd[1]);
	}
	if (panel_dev->dvdd_lcd_1v8) {
		regulator_disable(panel_dev->dvdd_lcd_1v8);
	}
	amati_panel_info(panel_dev, "*** Done with dsi_b_1440x1600_35_disable");
	return 0;
}

static int dsi_b_1440x1600_35_postpoweron(struct amati_panel_device *panel_dev)
{
	amati_panel_info(panel_dev, "*** dsi_b_1440x1600_35_postpoweron");
	if (panel_dev->vdd_lcd_bl_en) {
		int err = regulator_enable(panel_dev->vdd_lcd_bl_en);
		if (err < 0) {
			amati_panel_err(
				panel_dev,
				"vdd_lcd_bl_en regulator enable failed\n");
			return err;
		}
	}
	return 0;
}

static int dsi_b_1440x1600_35_prepoweroff(struct amati_panel_device *panel_dev)
{
	amati_panel_info(panel_dev, "*** dsi_b_1440x1600_35_prepoweroff\n");
	return 0;
}
static int dsi_b_1440x1600_35_postsuspend(struct amati_panel_device *panel_dev)
{
	amati_panel_info(panel_dev, "*** dsi_b_1440x1600_35_postsuspend\n");
	return 0;
}
/* Macros to reduce duplication in DC-specific wrapper functions */
#define AMATI_GET_AND_CHECK_PANEL_DEV(DC, FUNCNAME)                            \
	struct amati_panel_device *panel_dev = &(amati_panel_devs[DC]);        \
	do {                                                                   \
		if (!panel_dev->populated) {                                   \
			pr_err("*** " FUNCNAME                                 \
			       ": panel device not populated!\n");             \
			return -EINVAL;                                        \
		}                                                              \
	} while (0)

/******** DC-specific wrapper functions: DC 0 ********/
static int dsi_b_1440x1600_35_disable_dc0(struct device *dev)
{
	AMATI_GET_AND_CHECK_PANEL_DEV(0, "dsi_b_1440x1600_35_disable_dc0");
	BUG_ON(panel_dev->dc_dev != dev);
	return dsi_b_1440x1600_35_disable(panel_dev);
}
static int dsi_b_1440x1600_35_postpoweron_dc0(struct device *dev)
{
	AMATI_GET_AND_CHECK_PANEL_DEV(0, "dsi_b_1440x1600_35_postpoweron_dc0");
	BUG_ON(panel_dev->dc_dev != dev);
	return dsi_b_1440x1600_35_postpoweron(panel_dev);
}
static int dsi_b_1440x1600_35_prepoweroff_dc0(void)
{
	AMATI_GET_AND_CHECK_PANEL_DEV(0, "dsi_b_1440x1600_35_prepoweroff_dc0");
	return dsi_b_1440x1600_35_prepoweroff(panel_dev);
}
static int dsi_b_1440x1600_35_postsuspend_dc0(void)
{
	AMATI_GET_AND_CHECK_PANEL_DEV(0, "dsi_b_1440x1600_35_postsuspend_dc0");
	return dsi_b_1440x1600_35_postsuspend(panel_dev);
}
/******** DC-specific wrapper functions: DC 1 ********/
static int dsi_b_1440x1600_35_disable_dc1(struct device *dev)
{
	AMATI_GET_AND_CHECK_PANEL_DEV(1, "dsi_b_1440x1600_35_disable_dc1");
	BUG_ON(panel_dev->dc_dev != dev);
	return dsi_b_1440x1600_35_disable(panel_dev);
}
static int dsi_b_1440x1600_35_postpoweron_dc1(struct device *dev)
{
	AMATI_GET_AND_CHECK_PANEL_DEV(1, "dsi_b_1440x1600_35_postpoweron_dc1");
	BUG_ON(panel_dev->dc_dev != dev);
	return dsi_b_1440x1600_35_postpoweron(panel_dev);
}
static int dsi_b_1440x1600_35_prepoweroff_dc1(void)
{
	AMATI_GET_AND_CHECK_PANEL_DEV(1, "dsi_b_1440x1600_35_prepoweroff_dc1");
	return dsi_b_1440x1600_35_prepoweroff(panel_dev);
}
static int dsi_b_1440x1600_35_postsuspend_dc1(void)
{
	AMATI_GET_AND_CHECK_PANEL_DEV(1, "dsi_b_1440x1600_35_postsuspend_dc1");
	return dsi_b_1440x1600_35_postsuspend(panel_dev);
}

/***** Per config enable+populate, with ops structures *****/
static int dsi_b_1440x1600_35_enable_panela(struct device *dev)
{
	/* Panel A: always on DC 0, if present. */
	return dsi_b_1440x1600_35_enable(&amati_panel_devs[0], dev,
					 &amati_panel_opts[AMATI_PANEL_A]);
}

struct tegra_panel_ops dsi_b_1440x1600_35_a_ops = {
	.enable = dsi_b_1440x1600_35_enable_panela,
	.disable = dsi_b_1440x1600_35_disable_dc0,
	.postpoweron = dsi_b_1440x1600_35_postpoweron_dc0,
	.prepoweroff = dsi_b_1440x1600_35_prepoweroff_dc0,
	.postsuspend = dsi_b_1440x1600_35_postsuspend_dc0,
};
#if 0
	.hotplug_init = NULL,
	.hotplug_report = NULL
	// this one doesn't get used anymore on tx2!
	.pwm_bl_ops = &dsi_b_1440x1600_35_pwm_bl_ops,
#endif

static int dsi_b_1440x1600_35_enable_panelb(struct device *dev)
{
	/* Panel B: always on DC 1, if present. */
	return dsi_b_1440x1600_35_enable(&amati_panel_devs[1], dev,
					 &amati_panel_opts[AMATI_PANEL_B]);
}

struct tegra_panel_ops dsi_b_1440x1600_35_b_ops = {
	.enable = dsi_b_1440x1600_35_enable_panelb,
	.disable = dsi_b_1440x1600_35_disable_dc1,
	.postpoweron = dsi_b_1440x1600_35_postpoweron_dc1,
	.prepoweroff = dsi_b_1440x1600_35_prepoweroff_dc1,
	.postsuspend = dsi_b_1440x1600_35_postsuspend_dc1,
};

static int dsi_b_1440x1600_35_enable_duallink(struct device *dev)
{
	/* Dual-link panels config: always on DC 0, if present. */
	return dsi_b_1440x1600_35_enable(&amati_panel_devs[0], dev,
					 &amati_panel_opts[AMATI_DUAL_LINK]);
}
struct tegra_panel_ops dsi_b_1440x1600_35_duallink_ops = {
	.enable = dsi_b_1440x1600_35_enable_duallink,
	.disable = dsi_b_1440x1600_35_disable_dc0,
	.postpoweron = dsi_b_1440x1600_35_postpoweron_dc0,
	.prepoweroff = dsi_b_1440x1600_35_prepoweroff_dc0,
	.postsuspend = dsi_b_1440x1600_35_postsuspend_dc0,
};
